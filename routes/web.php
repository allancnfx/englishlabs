<?php

Route::get('/', function () {
    return redirect('/pt');
});

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'as' => 'admin.'], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('home', 'HomeController@index')->name('home');

    //Admin Login
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    //Admin Register
    Route::get('register', 'Auth\RegisterController@showRegistrationForm');
    Route::post('register', 'Auth\RegisterController@register');

    //Admin Passwords
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');

    // Rotas de Banners
    Route::get('banners', 'BannersController@index')->name('banners');
    Route::post('banners/reorder', 'BannersController@sortable')->name('banners.reorder');
    Route::post('banners/store', 'BannersController@store')->name('banners.store');
    Route::post('banners/update/{id}', 'BannersController@update')->name('banners.update.{id}');
    Route::get('banners/editar/{id}', 'BannersController@edit')->name('banners.editar.{id}');
    Route::get('banners/delete/{id}', 'BannersController@destroy')->name('banners.delete.{id}');

    // Rotas de quem somos
    Route::get('about', 'AboutController@index')->name('about');
    Route::post('about/reorder', 'AboutController@sortable')->name('about.reorder');
    Route::post('about/store', 'AboutController@store')->name('about.store');
    Route::post('about/store/{id}', 'AboutController@store')->name('about.store.{id}');

    // Rotas de Banners
    Route::get('bannerscx', 'BannerscxController@index')->name('bannerscx');
    Route::post('bannerscx/reorder', 'BannerscxController@sortable')->name('bannerscx.reorder');
    Route::post('bannerscx/store', 'BannerscxController@store')->name('bannerscx.store');
    Route::post('bannerscx/update/{id}', 'BannerscxController@update')->name('bannerscx.update.{id}');
    Route::get('bannerscx/editar/{id}', 'BannerscxController@edit')->name('bannerscx.editar.{id}');
    Route::get('bannerscx/delete/{id}', 'BannerscxController@destroy')->name('bannerscx.delete.{id}');

    // Rotas de Banners
    Route::get('bannersfr', 'BannersfrController@index')->name('bannersfr');
    Route::post('bannersfr/reorder', 'BannersfrController@sortable')->name('bannersfr.reorder');
    Route::post('bannersfr/store', 'BannersfrController@store')->name('bannersfr.store');
    Route::post('bannersfr/update/{id}', 'BannersfrController@update')->name('bannersfr.update.{id}');
    Route::get('bannersfr/editar/{id}', 'BannersfrController@edit')->name('bannersfr.editar.{id}');
    Route::get('bannersfr/delete/{id}', 'BannersfrController@destroy')->name('bannersfr.delete.{id}');


    // Rotas de Staff
    Route::get('staff', 'StaffController@index')->name('staff');
    Route::post('staff/reorder', 'StaffController@sortable')->name('staff.reorder');
    Route::post('staff/store', 'StaffController@store')->name('staff.store');
    Route::post('staff/update/{id}', 'StaffController@update')->name('staff.update.{id}');
    Route::get('staff/editar/{id}', 'StaffController@edit')->name('staff.editar.{id}');
    Route::get('staff/delete/{id}', 'StaffController@destroy')->name('staff.delete.{id}');

    // Rotas de process
    Route::get('stafftext', 'StaffTextController@index')->name('stafftext');
    Route::post('stafftext/store', 'StaffTextController@store')->name('stafftext.store');
    Route::post('stafftext/store/{id}', 'StaffTextController@store')->name('stafftext.store.{id}');

    // Rotas de PeloMundo
    Route::get('pelomundo', 'PeloMundoController@index')->name('pelomundo');
    Route::post('pelomundo/reorder', 'PeloMundoController@sortable')->name('pelomundo.reorder');
    Route::post('pelomundo/store', 'PeloMundoController@store')->name('pelomundo.store');
    Route::post('pelomundo/update/{id}', 'PeloMundoController@update')->name('pelomundo.update.{id}');
    Route::get('pelomundo/editar/{id}', 'PeloMundoController@edit')->name('pelomundo.editar.{id}');
    Route::get('pelomundo/delete/{id}', 'PeloMundoController@destroy')->name('pelomundo.delete.{id}');

    // Rotas de process
    Route::get('pelomundotext', 'PeloMundoTextController@index')->name('pelomundotext');
    Route::post('pelomundotext/store', 'PeloMundoTextController@store')->name('pelomundotext.store');
    Route::post('pelomundotext/store/{id}', 'PeloMundoTextController@store')->name('pelomundotext.store.{id}');

    // Rotas de process Description
    Route::get('processdesc', 'ProcessDescController@index')->name('processdesc');
    Route::post('processdesc/reorder', 'ProcessDescController@sortable')->name('processdesc.reorder');
    Route::post('processdesc/store', 'ProcessDescController@store')->name('processdesc.store');
    Route::post('processdesc/update/{id}', 'ProcessDescController@update')->name('processdesc.update.{id}');
    Route::get('processdesc/editar/{id}', 'ProcessDescController@edit')->name('processdesc.editar.{id}');
    Route::get('processdesc/delete/{id}', 'ProcessDescController@destroy')->name('processdesc.delete.{id}');

    
    // Rotas processos gráfico
    Route::get('processgrafic', 'ProcessGraficController@index')->name('processgrafic');
    Route::post('processgrafic/reorder', 'ProcessGraficController@sortable')->name('processgrafic.reorder');
    Route::post('processgrafic/store', 'ProcessGraficController@store')->name('processgrafic.store');
    Route::post('processgrafic/update/{id}', 'ProcessGraficController@update')->name('processgrafic.update.{id}');
    Route::get('processgrafic/editar/{id}', 'ProcessGraficController@edit')->name('processgrafic.editar.{id}');
    Route::get('processgrafic/delete/{id}', 'ProcessGraficController@destroy')->name('processgrafic.delete.{id}');


    // Rotas de process
    Route::get('process', 'ProcessController@index')->name('process');
    Route::post('process/reorder', 'ProcessController@sortable')->name('process.reorder');
    Route::post('process/store', 'ProcessController@store')->name('process.store');
    Route::post('process/store/{id}', 'ProcessController@store')->name('process.store.{id}');

    // Rotas de event description
    Route::get('eventdesc', 'EventDescController@index')->name('eventsdesc');
    Route::post('eventdesc/reorder', 'EventDescController@sortable')->name('eventdesc.reorder');
    Route::post('eventdesc/store', 'EventDescController@store')->name('eventdesc.store');
    Route::post('eventdesc/update/{id}', 'EventDescController@update')->name('eventdesc.update.{id}');
    Route::get('eventdesc/editar/{id}', 'EventDescController@edit')->name('eventdesc.editar.{id}');
    Route::get('eventdesc/delete/{id}', 'EventDescController@destroy')->name('eventdesc.delete.{id}');

    // Rotas de event
    Route::get('event', 'EventController@index')->name('event');
    Route::post('event/reorder', 'EventController@sortable')->name('event.reorder');
    Route::post('event/store', 'EventController@store')->name('event.store');
    Route::post('event/store/{id}', 'EventController@store')->name('event.store.{id}');

    // Rotas de event description
    Route::get('campaignsdesc', 'CampaignsDescController@index')->name('campaignsdesc');
    Route::post('campaignsdesc/reorder', 'CampaignsDescController@sortable')->name('campaignsdesc.reorder');
    Route::post('campaignsdesc/store', 'CampaignsDescController@store')->name('campaignsdesc.store');
    Route::post('campaignsdesc/update/{id}', 'CampaignsDescController@update')->name('campaignsdesc.update.{id}');
    Route::get('campaignsdesc/editar/{id}', 'CampaignsDescController@edit')->name('campaignsdesc.editar.{id}');
    Route::get('campaignsdesc/delete/{id}', 'CampaignsDescController@destroy')->name('campaignsdesc.delete.{id}');

    // Rotas de event
    Route::get('campaigns', 'CampaignsController@index')->name('event');
    Route::post('campaigns/reorder', 'CampaignsController@sortable')->name('campaigns.reorder');
    Route::post('campaigns/store', 'CampaignsController@store')->name('campaigns.store');
    Route::post('campaigns/store/{id}', 'CampaignsController@store')->name('campaigns.store.{id}');

    // Rotas de campaigns description
    Route::get('campaignsdesc', 'CampaignsDescController@index')->name('campaignsdesc');
    Route::post('campaignsdesc/reorder', 'CampaignsDescController@sortable')->name('campaignsdesc.reorder');
    Route::post('campaignsdesc/store', 'CampaignsDescController@store')->name('campaignsdesc.store');
    Route::post('campaignsdesc/update/{id}', 'CampaignsDescController@update')->name('campaignsdesc.update.{id}');
    Route::get('campaignsdesc/editar/{id}', 'CampaignsDescController@edit')->name('campaignsdesc.editar.{id}');
    Route::get('campaignsdesc/delete/{id}', 'CampaignsDescController@destroy')->name('campaignsdesc.delete.{id}');

    // Rotas de campaigns
    Route::get('campaigns', 'CampaignsController@index')->name('event');
    Route::post('campaigns/reorder', 'CampaignsController@sortable')->name('campaigns.reorder');
    Route::post('campaigns/store', 'CampaignsController@store')->name('campaigns.store');
    Route::post('campaigns/store/{id}', 'CampaignsController@store')->name('campaigns.store.{id}');

    // Rotas de depositions description
    Route::get('depositionsdesc', 'DepositionsDescController@index')->name('depositionsdesc');
    Route::post('depositionsdesc/reorder', 'DepositionsDescController@sortable')->name('depositionsdesc.reorder');
    Route::post('depositionsdesc/store', 'DepositionsDescController@store')->name('depositionsdesc.store');
    Route::post('depositionsdesc/update/{id}', 'DepositionsDescController@update')->name('depositionsdesc.update.{id}');
    Route::get('depositionsdesc/editar/{id}', 'DepositionsDescController@edit')->name('depositionsdesc.editar.{id}');
    Route::get('depositionsdesc/delete/{id}', 'DepositionsDescController@destroy')->name('depositionsdesc.delete.{id}');

    // Rotas de depositions
    Route::get('depositions', 'DepositionsController@index')->name('event');
    Route::post('depositions/reorder', 'DepositionsController@sortable')->name('depositions.reorder');
    Route::post('depositions/store', 'DepositionsController@store')->name('depositions.store');
    Route::post('depositions/store/{id}', 'DepositionsController@store')->name('depositions.store.{id}');

    // Rotas de quem somos
    Route::get('contact', 'ContactController@index')->name('contact');
    Route::post('contact/reorder', 'ContactController@sortable')->name('contact.reorder');
    Route::post('contact/store', 'ContactController@store')->name('contact.store');
    Route::post('contact/store/{id}', 'ContactController@store')->name('contact.store.{id}');

    // Rotas de quem somos descrição
    Route::get('whatofferdesc', 'WhatofferDescController@index')->name('whatofferdesc');
    Route::post('whatofferdesc/reorder', 'WhatofferDescController@sortable')->name('whatofferdesc.reorder');
    Route::post('whatofferdesc/store', 'WhatofferDescController@store')->name('whatofferdesc.store');
    Route::post('whatofferdesc/update/{id}', 'WhatofferDescController@update')->name('whatofferdesc.update.{id}');
    Route::get('whatofferydesc/editar/{id}', 'WhatofferDescController@edit')->name('whatofferdesc.editar.{id}');
    Route::get('whatofferdesc/delete/{id}', 'WhatofferDescController@destroy')->name('whatofferdesc.delete.{id}');

    // Rotas de process
    Route::get('proficiency', 'ProficiencyController@index')->name('proficiency');
    Route::post('proficiency/reorder', 'ProficiencyController@sortable')->name('proficiency.reorder');
    Route::post('proficiency/store', 'ProficiencyController@store')->name('proficiency.store');
    Route::post('proficiency/store/{id}', 'ProficiencyController@store')->name('proficiency.store.{id}');


     // Rotas de process
    Route::get('popup', 'PopupController@index')->name('popup');
    Route::post('popup/reorder', 'PopupController@sortable')->name('popup.reorder');
    Route::post('popup/store', 'PopupController@store')->name('popup.store');
    Route::post('popup/store/{id}', 'PopupController@store')->name('popup.store.{id}');
});
Route::get('/', 'HomeController@index')->name('home');
Route::post('/send', 'HomeController@sendContact')->name('send');

