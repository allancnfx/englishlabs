<?php

/**
 * Created by PhpStorm.
 * User: Will
 * Date: 02/08/2016
 * Time: 17:30
 */

namespace App\Services;

use File;
use Image;

class StandardService {

    public static function doUpload($file, $path, $resize, $name) {

        // Verifica se o arquivo foi fornecida
        if (empty($file))
            return false;

        // Cria o diretório de upload caso não exista
        if (!File::exists($path))
            File::makeDirectory($path, 0777, true);

        // Gera o nome do arquivo e move-o para o diretório de upload
        $extension = $file->getClientOriginalExtension();

        if ($name) {
            $filename = $name . '-' . time() . '.' . $extension;
        } else {
            $filename = uniqid() . '-' . time() . '.' . $extension;
        }

        $path = public_path($path . $filename);
        if ($resize != false) {
            Image::make($file->getRealPath())->resize($resize, null, function($constraint) {

                $constraint->aspectRatio();
            })->save($path);
        } else {
            Image::make($file->getRealPath())->save($path);
        }

        return $filename;
    }

    public static function anyFile($file, $path, $name) {
        // Verifica se o arquivo foi fornecida
        if (empty($file))
            return false;

        // Cria o diretório de upload caso não exista
        if (!File::exists($path))
            File::makeDirectory($path, 0777, true);

        // Gera o nome do arquivo e move-o para o diretório de upload
        $filename = $name;

        $file->move($path, $filename);

        return $filename;
    }

    public function createNameFile($file) {
        $convert_accents = str_slug($file->getClientOriginalName(), '-');
        $extension = $file->getClientOriginalExtension();

        $removeLastLetter = substr($convert_accents, 0, -3);
        $result = $removeLastLetter . '.' . $extension;

        return $result;
    }

    public static function doReorder($items = [], $grid = false, $model) {
        if (count($items) == 0)
            $items = $model->all();

        $order = count($items);
        foreach ($items as $item) {
            $item = $model->find($grid ? $item['id'] : $item->id);
            $item->order = $order;
            $item->save();
            $order--;
        }
    }


}
