<?php

namespace App\Http\Controllers\Admin;

use App\Entities\WhatofferDesc;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;
use App\Http\Requests;
use File;
use Response;
use Redirect;
use Session;

class WhatofferDescController extends Controller {

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(StandardService $standard) {
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $items = WhatofferDesc::all();
        return view('admin.whatofferdesc.index', compact('items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    public function store(Request $request) {
        try {
            $item = new WhatofferDesc;

            $image = $this->standard->doUpload($request->file('file_image'), '../_files/proficiencia/', false, false);
            $item->image = $image;

            $item->desc = $request->input('desc');

            $item->popup = $request->input('popup');



            $item->save();

            Session::flash('success', 'Item criado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/admin/whatofferdesc')->with('status', 'Cadastro realizado com sucesso!');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function edit($id = 0) {
        $item = WhatofferDesc::find($id);
        return view('admin.whatofferdesc.edit', compact('item'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request, $id = 0) {
        try {
            $item = WhatofferDesc::find($id);

            if (!empty($request->file('file_image'))) {
                File::delete('_files/proficiencia/' . $item->image);
                $image_name = $this->standard->doUpload($request->file('file_image'), '../_files/proficiencia/', false, false);
                $item->image = $image_name;
            }

            $item->desc = $request->input('desc');

            $item->popup = $request->input('popup');

            $item->save();

            Session::flash('success', 'Item atualizado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/admin/whatofferdesc')->with('status', 'Imagem atualizada com sucesso!');
    }

    /**
     * @param Request $request
     * @return int
     */
    public function sortable(Request $request) {
        $this->standard->doReorder($request->input('items'), true, $this->repository);
        return 2;
    }

    /**
     * @param $id
     * @return int
     */
    public function destroy($id) {
        $item = WhatofferDesc::find($id);

        if (!empty($item->image)) {
            File::delete('../_files/proficiencia/' . $item->image);
        }

        $item->delete();
        return Redirect::to('/admin/whatofferdesc')->with('status', 'Imagem excluída com sucesso!');
    }

}
