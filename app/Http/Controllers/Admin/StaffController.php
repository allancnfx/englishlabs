<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Staff;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;
use App\Http\Requests;
use File;
use Response;
use Redirect;
use Session;

class StaffController extends Controller {

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(StandardService $standard) {
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $items = Staff::orderBy('order','desc')->get();
        return view('admin.staff.index', compact('items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    public function store(Request $request) {
        try {
            $item = new Staff;

            $image = $this->standard->doUpload($request->file('file_image'), '../_files/staff/', false, false);
            $item->image = $image;

            $item->title = $request->input('title');

            $item->desc = $request->input('desc');

            $item->link_face = $request->input('link_face');

            $item->link_insta = $request->input('link_insta');

            $item->save();

            Session::flash('success', 'Item criado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/admin/staff')->with('status', 'Cadastro realizado com sucesso!');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function edit($id = 0) {
        $item = Staff::find($id);
        return view('admin.staff.edit', compact('item'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request, $id = 0) {
        try {
            $item = Staff::find($id);

            if (!empty($request->file('file_image'))) {
                File::delete('../_files/staff/' . $item->image);
                $image_name = $this->standard->doUpload($request->file('file_image'), '../_files/staff/', false, false);
                $item->image = $image_name;
            }
            $item->title = $request->input('title');

            $item->desc = $request->input('desc');

            $item->link_face = $request->input('link_face');

            $item->link_insta = $request->input('link_insta');
            
            $item->save();

            Session::flash('success', 'Item atualizado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/admin/staff')->with('status', 'Imagem atualizada com sucesso!');
    }

    /**
     * @param Request $request
     * @return int
     */
    public function sortable(Request $request) {
        $this->standard->doReorder($request->input('items'), true, new Staff);
        return 2;
    }

    /**
     * @param $id
     * @return int
     */
    public function destroy($id) {
        $item = Staff::find($id);

        if (!empty($item->image))
            File::delete('../_files/staff/' . $item->image);

        $item->delete();
        return Redirect::to('/admin/staff')->with('status', 'Imagem excluída com sucesso!');
    }
    
}
