<?php

namespace App\Http\Controllers\Admin;

use App\Entities\DepositionsDesc;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;
use App\Http\Requests;
use File;
use Response;
use Redirect;
use Session;

class DepositionsDescController extends Controller {

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(StandardService $standard) {
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $items = DepositionsDesc::orderBy('order','desc')->get();
        return view('admin.depositionsdesc.index', compact('items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    public function store(Request $request) {
        try {
            $item = new DepositionsDesc;

            $image = $this->standard->doUpload($request->file('file_image'), '../_files/depoimentos/', false, false);
            $item->image = $image;

            $item->title = $request->input('title');

            $item->desc = $request->input('desc');

            $item->save();

            Session::flash('success', 'Item criado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/admin/depositionsdesc')->with('status', 'Cadastro realizado com sucesso!');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function edit($id = 0) {
        $item = DepositionsDesc::find($id);
        return view('admin.depositionsdesc.edit', compact('item'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request, $id = 0) {
        try {
            $item = DepositionsDesc::find($id);

            if (!empty($request->file('file_image'))) {
                File::delete('../_files/depoimentos/' . $item->image);
                $image_name = $this->standard->doUpload($request->file('file_image'), '../_files/depoimentos/', false, false);
                $item->image = $image_name;
            }
            $item->title = $request->input('title');
            
            $item->desc = $request->input('desc');

            $item->save();

            Session::flash('success', 'Item atualizado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/admin/depositionsdesc')->with('status', 'Imagem atualizada com sucesso!');
    }

    /**
     * @param Request $request
     * @return int
     */
    public function sortable(Request $request) {
        $this->standard->doReorder($request->input('items'), true, new DepositionsDesc);
        return 2;
    }

    /**
     * @param $id
     * @return int
     */
    public function destroy($id) {
        $item = DepositionsDesc::find($id);

        if (!empty($item->image)) {
            File::delete('../_files/depoimentos/' . $item->image);
        }

        $item->delete();
        return Redirect::to('/admin/depositionsdesc')->with('status', 'Imagem excluída com sucesso!');
    }

}
