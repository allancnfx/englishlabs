<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Maker;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;
use App\Http\Requests;
use File;
use Response;
use Redirect;
use Session;

class PeloMundoController extends Controller {

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(StandardService $standard) {
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $items = Maker::all();
        return view('admin.pelomundo.index', compact('items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    public function store(Request $request) {
        try {
            $item = new Maker;

            $item->title = $request->input('title');

            $item->latitude = $request->input('latitude');
            $item->longitude = $request->input('longitude');

            $image = $this->standard->doUpload($request->file('file_image'), '_files/pelomundo/', false, false);
            $item->image = $image;


            $item->save();

            Session::flash('success', 'Item criado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/admin/pelomundo')->with('status', 'Cadastro realizado com sucesso!');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function edit($id = 0) {
        $item = Maker::find($id);
        return view('admin.pelomundo.edit', compact('item'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request, $id = 0) {
        try {
            $item = Maker::find($id);
            $item->title = $request->input('title');
            $item->latitude = $request->input('latitude');
            $item->longitude = $request->input('longitude');
            if (!empty($request->file('file_image'))) {
                File::delete('_files/pelomundo/' . $item->image);
                $image_name = $this->standard->doUpload($request->file('file_image'), '_files/pelomundo/', false, false);
                $item->image = $image_name;
            }
            $item->save();

            Session::flash('success', 'Item atualizado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/admin/pelomundo')->with('status', 'Imagem atualizada com sucesso!');
    }

    /**
     * @param $id
     * @return int
     */
    public function destroy($id) {
        $item = Maker::find($id);

        if (!empty($item->image)) {
            File::delete('../_files/pelomundo/' . $item->image);
        }

        $item->delete();
        return Redirect::to('/admin/pelomundo')->with('status', 'Imagem excluída com sucesso!');
    }

}
