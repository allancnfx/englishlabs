<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Bannerscx;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;
use App\Http\Requests;
use File;
use Response;
use Redirect;
use Session;

class BannerscxController extends Controller {

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(StandardService $standard) {
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $items = Bannerscx::all();
        return view('admin.about.banners_caxias.index', compact('items'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    public function store(Request $request) {
        try {
            $item = new Bannerscx;

            $image = $this->standard->doUpload($request->file('file_image'), '../_files/bannerscx/', false, false);
            $item->image = $image;


            $item->save();

            Session::flash('success', 'Item criado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/admin/bannerscx')->with('status', 'Cadastro realizado com sucesso!');
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function edit($id = 0) {
        $item = Bannerscx::find($id);
        return view('admin.about.banners_caxias.edit', compact('item'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @return mixed
     */
    public function update(Request $request, $id = 0) {
        try {
            $item = Bannerscx::find($id);

            if (!empty($request->file('file_image'))) {
                File::delete('../_files/bannerscx/' . $item->image);
                $image_name = $this->standard->doUpload($request->file('file_image'), '../_files/bannerscx/', false, false);
                $item->image = $image_name;
            }
            $item->save();

            Session::flash('success', 'Item atualizado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/admin/bannerscx')->with('status', 'Imagem atualizada com sucesso!');
    }

    /**
     * @param Request $request
     * @return int
     */
    public function sortable(Request $request) {
        $this->standard->doReorder($request->input('items'), true, $this->repository);
        return 2;
    }

    /**
     * @param $id
     * @return int
     */
    public function destroy($id) {
        $item = Bannerscx::find($id);

        if (!empty($item->image))
            File::delete('../_files/bannerscx/' . $item->image);

        $item->delete();
        return Redirect::to('/admin/bannerscx')->with('status', 'Imagem excluída com sucesso!');
    }

}
