<?php

namespace App\Http\Controllers\Admin;

use App\Entities\About;
use App\Http\Controllers\Controller;
use App\Services\StandardService;
use Illuminate\Http\Request;
use Redirect;
use Session;

class AboutController extends Controller {

    /**
     * @var StandardService
     */
    protected $standard;

    public function __construct(StandardService $standard) {
        $this->standard = $standard;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $item = About::get()->first();
        return view('admin.about.index', compact('item'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    // SALVANDO
    /**
     * @param Request $request
     * @return mixed
     */
    public function store(Request $request, $id = 0) {
        try {

            if ($id == 0) {
                $item = new About;
            } else {
                $item = About::find($id);
            }


            $item->title_about = $request->input('title_about');
            
            $item->text_about = $request->input('text_about');
            
            $item->textcx = $request->input('textcx');
            
            $item->textfr = $request->input('textfr');
            

            $item->save();

            Session::flash('success', 'Item criado com sucesso!');
        } catch (ValidatorException $e) {

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
        return Redirect::to('/admin/about')->with('status', 'Post realizado com sucesso!');
    }


    /**
     * @param Request $request
     * @return int
     */
    public function sortable(Request $request) {
        $this->standard->doReorder($request->input('items'), true, $this->repository);
        return 2;
    }

   

}
