<?php

namespace App\Http\Controllers;

use App\Entities\About;
use App\Entities\Banners;
use App\Entities\Bannerscx;
use App\Entities\Bannersfr;
use App\Entities\Contact;
use App\Entities\Depositions;
use App\Entities\DepositionsDesc;
use App\Entities\Event;
use App\Entities\EventDesc;
use App\Entities\Maker;
use App\Entities\PeloMundoText;
use App\Entities\Process;
use App\Entities\ProcessDesc;
use App\Entities\WhatofferDesc;
use App\Entities\ProcessGrafic;
use App\Entities\Proficiency;
use App\Entities\Staff;
use App\Entities\Popup;
use App\Entities\StaffText;
use Mail;
use Session;
use Redirect;
use App\Mail\ContactSend;
use Illuminate\Http\Request;
use JavaScript;
use Illuminate\Support\Facades\View;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return \App\Http\Controllers\HomeController
     */
    public function index() {
        $data = array(
            'banners' => Banners::all(),
            'about' => About::first(),
            'bannerscx' => Bannerscx::all(),
            'bannersfr' => Bannersfr::all(),
            'staffs' => Staff::orderBy('order','desc')->get(),
            'stafftext' => StaffText::first(),
            'processtext' => Process::get()->first(),
            'proficiencytext' => Proficiency::first(),
            'pelomundotext' => PeloMundoText::first(),
            'processitems' => ProcessDesc::all(),
            'proficiencyitems' => WhatofferDesc::all(),
            'processgrafic' => ProcessGrafic::all(),
            'eventtext' => Event::get()->first(),
            'events' => EventDesc::all(),
            'depositionstext' => Depositions::get()->first(),
            'depositionsdesc' => DepositionsDesc::orderBy('order','desc')->get(),
            'popup' => Popup::first(),
            'contatotext' => Contact::get()->first()
        );

        JavaScript::put([
            'icon_makers' => Maker::all()
        ]);



        return View::first(['home'], compact('data'));
    }


    /**
     * Ship the given order.
     *
     * @param  Request  $request
     * @return Response
     */
    public function sendContact(Request $request) {
        Mail::to('contato@englishlabs.com.br')
                ->send(new ContactSend($request));
        if (count(Mail::failures()) > 0) {

            Session::flash('error', 'Falha ao enviar a mensagem, tente novamente mais tarde!');
            return Redirect::to('/' . '#contato');
        } else {
            Session::flash('success', 'Sua mensagem foi enviada com sucesso!');
            return Redirect::to('/' . '#contato');
        }
    }

}
