<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

class ContactSend extends Mailable {

    use Queueable,
        SerializesModels;

    /**
     * The request instance.
     *
     */
    protected $request;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request) {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    
    public function build() {
        return $this->from('assistencia@compuoffice.com.br')
                        ->view('emails.contato')
                        ->with([
                            'requestName' => $this->request->name,
                            'requestPhone' => $this->request->phone,
                            'requestEmail' => $this->request->email,
                            'requestText' => $this->request->text,
        ]);
    }

}
