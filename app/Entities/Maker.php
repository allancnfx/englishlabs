<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Maker extends Model
{
    protected $table = 'makers';
    protected $fillable = ['title', 'latitude','longitude', 'image'];
}