<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class EventDesc extends Model
{
    protected $table = 'eventdesc';
    protected $fillable = ['image', 'data', 'desc', 'text'];
}