<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class DepositionsDesc extends Model
{
    protected $table = 'depositionsdesc';
    protected $fillable = ['image', 'title', 'desc', 'order'];
}