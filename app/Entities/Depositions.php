<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Depositions extends Model
{
    protected $table = 'depositions';
    protected $fillable = ['title', 'desc'];
}