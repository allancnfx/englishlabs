<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Bannerscx extends Model
{
    protected $table = 'bannerscx';
    protected $fillable = ['image'];
}