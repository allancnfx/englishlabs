<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Popup extends Model
{
    protected $table = 'popup';
    protected $fillable = ['title','desc','desc_mobile'];
}