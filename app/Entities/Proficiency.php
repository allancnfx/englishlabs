<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Proficiency extends Model
{
    protected $table = 'proficiency';
    protected $fillable = ['title', 'desc', 'call'];
}