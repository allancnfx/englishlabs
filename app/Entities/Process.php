<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Process extends Model
{
    protected $table = 'process';
    protected $fillable = ['title', 'desc'];
}