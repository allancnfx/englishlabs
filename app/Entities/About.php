<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $table = 'about';
    protected $fillable = ['title_about', 'text_about', 'textcx', 'textfr'];
}