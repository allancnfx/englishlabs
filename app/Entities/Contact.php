<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contact';
    protected $fillable = ['title', 'contact_desc', 'desc', 'email', 'adress_cx', 'tel_cx', 'adress_fr', 'tel_fr', 'titleform', 'whatss'];
}