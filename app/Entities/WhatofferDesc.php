<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class WhatofferDesc extends Model
{
    protected $table = 'proficiencydesc';
    protected $fillable = ['image', 'desc', 'popup'];
}