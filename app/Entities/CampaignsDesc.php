<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class CampaignsDesc extends Model
{
    protected $table = 'campaignsdesc';
    protected $fillable = ['image', 'title', 'desc'];
}