<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('contact', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255)->nullable();
            $table->longText('contact_desc')->nullable();
            $table->longText('desc')->nullable();
            $table->string('email')->nullable();
            $table->longText('adress_cx')->nullable();
            $table->string('tel_cx')->nullable();
            $table->longText('adress_fr')->nullable();
            $table->string('tel_fr')->nullable();
            $table->longText('titleform')->nullable();
            $table->string('whatss')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('contact');
    }

}
