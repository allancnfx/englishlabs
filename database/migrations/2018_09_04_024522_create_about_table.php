<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('about', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title_about', 255)->nullable();
            $table->longText('text_about')->nullable();
            $table->longText('textcx')->nullable();
            $table->longText('textfr')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('about');
    }

}
