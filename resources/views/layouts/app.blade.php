<!doctype html>

    

<html lang="en">
    <head>
        <title>English Labs | Aulas Particulares</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!--header icon CSS -->
        <link rel="icon" href="{{ asset('front/images/favicon.png') }}">
        <!-- style CSS -->
        <link rel="stylesheet" href="{{ asset('front/css/style.css') }}">
        <!-- responsive CSS -->
        <link rel="stylesheet" href="{{ asset('front/css/responsive.css') }}">
        <!-- maps CSS -->
        <link rel="stylesheet" href="{{ asset('front/ammap/ammap.css') }}" type="text/css">
        <script>

            window.onbeforeunload = function() {
                return alert(1);
            };
        </script>
    </head>

    <body class="sticky-header sticky-footer">
        @include('layouts.header')
        @yield('content')
        @include('layouts.footer')
    </body>
    
</html>