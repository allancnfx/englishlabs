<header>
    <!--header area start-->
    <div class="header-area wow fadeInDown header-absolate" id="nav" data-0="position:fixed;" data-top-top="position:fixed;top:0;" data-edge-strategy="set">
        <div class="container">
            <div class="mobile-menu"></div>
           <dir class="row">
            <div class="col-4 col-md-1 col-lg-2">
                <div class="logo-area">
                    <a href="#"><img class="logo" src="{{ asset('front/images/logoenglishlabs-branco.png') }}" alt=""></a>
                </div>
            </div>
            <div class="col-6 col-md-11 col-lg-10 d-lg-block">
                <div class="main-menu text-center txt-uppercase">
                    <div class="close-menu">x fechar</div>
                    <nav class="navigation">
                        <ul id="slick-nav">
                            <li><a class="scroll" href="#home">Home</a></li>
                            <li><a class="scroll" href="#sobre">{{ !empty($data['about']->title_about) ? $data['about']->title_about : '' }}</a></li>
                            <li><a class="scroll" href="#nossos-processos">{{ !empty($data['processtext']->title) ? $data['processtext']->title : '' }}</a></li>
                            <li><a class="scroll" href="#proficiencia">{{ !empty($data['proficiencytext']->title) ? $data['proficiencytext']->title : '' }}</a></li>
                            <li><a class="scroll" href="#pelo-mundo">{{ !empty($data['pelomundotext']->title) ? $data['pelomundotext']->title : '' }}</a></li>
                            <li><a class="scroll" href="#staff">{{ !empty($data['stafftext']->title) ? $data['stafftext']->title : '' }}</a></li>
                            <li><a class="scroll" href="#depoimentos">{{ !empty($data['depositionstext']->title) ? $data['depositionstext']->title : '' }}</a></li>
                            <li><a class="scroll" href="#contato">{{ !empty($data['contatotext']->title) ? $data['contatotext']->title : '' }}</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </dir>
        </div>
    </div>
    <!--header area end-->
</header>