<!--footer area start-->


   


<footer class="footer-container">
    <div class="footera-area section-padding wow fadeInDown">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-4 col-lg-5">
                    <div class="logo-area footer">
                        <a href="#"><img src="{{ asset('front/images/logoenglishlabs-branco.png') }}" alt=""></a>
                    </div>
                    <div class="space-20"></div>
                    <p class="">Escola particular de idiomas. Inglês e espanhol.</p>
                    <div class="space-10"></div>
                    <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos direitos reservados | English Labs</p>
                </div>
                <div class="col-12 col-md-4 col-lg-3">
                    <div class="single-footer justify-content-end">
                        <ul>
                            <li><a class="scroll" href="#home">Home</a></li>
                            <li><a class="scroll" href="#sobre">{{ !empty($data['about']->title_about) ? $data['about']->title_about : '' }}</a></li>
                            <li><a class="scroll" href="#nossos-processos">{{ !empty($data['processtext']->title) ? $data['processtext']->title : '' }}</a></li>
                            <li><a class="scroll" href="#proficiencia">{{ !empty($data['proficiencytext']->title) ? $data['proficiencytext']->title : '' }}</a></li>
                            <li><a class="scroll" href="#pelo-mundo">{{ !empty($data['pelomundotext']->title) ? $data['pelomundotext']->title : '' }}</a></li>
                            <li><a class="scroll" href="#staff">{{ !empty($data['stafftext']->title) ? $data['stafftext']->title : '' }}</a></li>
                            <li><a class="scroll" href="#depoimentos">{{ !empty($data['depositionstext']->title) ? $data['depositionstext']->title : '' }}</a></li>
                            <li><a class="scroll" href="#contato">{{ !empty($data['contatotext']->title) ? $data['contatotext']->title : '' }}</a></li>
                        </ul>
                    </div>
                </div> 
                <div class="col-12 col-md-4 col-lg-4 contact-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="single-footer-social social-icons sc">
                                <a href="http://facebook.com/enjoyenglishlabs" target="_blank"><i class="fa fa-facebook"></i></a>
                                <a href="http://instagram.com/englishlabs" target="_blank"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="adress-footer">
                                <p class="text-center text-contat"><b>CAXIAS DO SUL
                                    @if(!empty($data['contatotext']->tel_cx))
                                     -  {{$data['contatotext']->tel_cx}}</b></p>
                                    @endif
                                <p class="text-center adress-cx">
                                    @if(!empty($data['contatotext']->adress_cx))
                                         {!!$data['contatotext']->adress_cx!!}
                                    @endif
                                </p>


                                <p class="text-center text-contat"><b>FARROUPILHA
                                    @if(!empty($data['contatotext']->tel_fr))
                                    - {{$data['contatotext']->tel_fr}}</b></p>                          
                                    @endif
                                <p class="text-center"> 
                                    @if(!empty($data['contatotext']->adress_fr))
                                       {{$data['contatotext']->adress_fr}}
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</footer>    
<!--footer area end-->
<!-- main js-->
<script src="{{ asset('front/js/jquery-2.2.4.min.js') }}"></script>

<script src="{{ asset('front/js/parallax.min.js') }}"></script>
<!--core js-->
<script src="{{ asset('front/js/core.js') }}"></script>
<!--theme js-->
<script src="{{ asset('front/js/theme.js') }}"></script>

<!-- popper js-->
<script src="{{ asset('front/js/popper.js') }}"></script>
<!-- carousel js-->
<script src="{{ asset('front/js/owl.carousel.min.js') }}"></script>
<!-- wow js-->
<script src="{{ asset('front/js/wow.min.js') }}"></script>
<!-- bootstrap js-->
<script src="{{ asset('front/js/bootstrap.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('front/js/bioep.js') }}"></script>

<!--mobile menu js-->
<script src="{{ asset('front/js/jquery.slicknav.min.js') }}"></script>
<!-- map file should be included after ammap.js -->
<script src="{{ asset('front/ammap/ammap.js') }}" type="text/javascript"></script>
<script src="{{ asset('front/ammap/maps/js/worldLow.js') }}" type="text/javascript"></script>
<script src="{{ asset('front/ammap/themes/black.js') }}" type="text/javascript"></script>

<!-- map file should be included after ammap.js -->
<script src="{{ asset('front/js/main.js') }}"></script>