@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
<h1>Staff</h1>
@stop


@section('content')
<div class="row">
    <div class="col-md-8 ">
        <div class="box ">
            <div class="box-header with-border">
                @if (!empty($items))
                <h3 class="box-title">Lista Staff</h3>
                @else
                <h3 class="box-title">Nenhum um item cadastrado</h3>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
                @endif
                <div class="col-md-12 color-palette">
                    <table id="tabela" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Título</th>
                                <th>Imagem</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody class="sortable" data-url="{{route('admin.staff.reorder')}}">
                            @foreach($items as $item)
                            <tr class="ui-state-default" data-id="{{$item->id}}">
                                <td>{{$item->id}}</td>
                                <td>{{$item->title}}</td>
                                <td><img width="80" src="{{asset('_files/staff/'.$item->image)}}"></td>
                                <td>
                                    <div class="btn-group">
                                        <a type="button" class="btn btn-success" href="{{route('admin.staff.editar.{id}', array($item->id))}}" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-pencil"></i></a>
                                        <a type="link"  class="btn btn-danger" href="{{route('admin.staff.delete.{id}', array($item->id))}}" data-toggle="tooltip" data-original-title="Deletar"><i class="fa fa-trash-o"></i></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>


                </div>
            </div>
            <!-- /.row -->
        </div>
    </div>
    <div class="col-xs-12 col-md-4">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Adicionar Banner</h3>
            </div>
            <form action="{{ route('admin.staff.store') }}" class="form-table-list" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="image">Imagem:</label>
                                <div class="upload">
                                    <div class="btn btn-primary">Adicionar Imagem</div>
                                    <input type="file" class="input-file" id="image" name="file_image" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Nome</label>
                                <input type="text" class="form-control" placeholder="Nome" name="title">
                            </div>
                            <div class="form-group">
                                <label>Descrição</label>
                                <input type="text" class="form-control" placeholder="Descrição" name="desc">
                            </div>
                            <div class="form-group">
                                <label>Facebook</label>
                                <input type="text" class="form-control" placeholder="Facebook" name="link_face">
                            </div>
                            <div class="form-group">
                                <label>Instagram</label>
                                <input type="text" class="form-control" placeholder="Instagram" name="link_insta">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-success btn-send pull-right">Salvar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>    
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('vendor/adminlte/css/main.css')}}">
@stop

@section('js')
<script src="{{ asset('js/jquery-ui.js') }}"></script>
<script>
    <!-- CSRF Token -->
    var csrf = "{{ csrf_token() }}";
    var App = App || {};
    App.sortableGrid = function () {
        $('.sortable').each(function (idx, el) {
            var sortable = $(el);
            var obj = {};
            var url = sortable.attr('data-url');
            sortable.sortable().on('sortupdate', function (event, ui) {
                var items = [];
                sortable.find('.ui-state-default').each(function (i) {
                    var order = sortable.find('.ui-state-default').length - i;
                    obj = {'id': $(this).data('id'), 'order': order};
                    items.push(obj);
                });
                App.reorder(items, url);
            });

            sortable.disableSelection();
        });
    };

    App.reorder = function (items, url) {
        if (items.length > 0) {
            $.ajax({
                url: url,
                headers: {'X-CSRF-Token': csrf},
                type: "POST",
                data: {items: items},
                success: function (data) {

                }
            });
        }
    };
    App.sortableGrid();
</script>
@stop