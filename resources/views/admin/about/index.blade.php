@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
<h1>Sobre nós</h1>
@stop


@section('content')
<div class="row">
    <div class="col-md-8 ">
        <div class="box ">
            <div class="box-header with-border">

                <div class="box box-warning">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="{{ empty($item->id) ? route('admin.about.store') : route('admin.about.store.{id}', array($item->id)) }}" class="form-table-list" method="POST" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <!-- text input -->
                            <div class="form-group">
                                <label>Título quem somos</label>
                                <input type="text" class="form-control" placeholder="Título" name="title_about" value="{{ empty($item->title_about) ? old('title_about') : $item->title_about}}">
                            </div>
                            <!-- textarea -->
                            <div class="form-group">
                                <label>Texto quem somos</label>
                                <textarea class="form-control summernote" rows="3" placeholder="Texto corpo" name="text_about">{{ empty($item->text_about) ? old('text_about') : $item->text_about}}</textarea>
                            </div>
                            
                            <!-- textarea -->
                            <div class="form-group">
                                <label>Texto Caxias do Sul</label>
                                <textarea class="form-control summernote" rows="3" placeholder="Texto corpo" name="textcx">{{ empty($item->textcx) ? old('textcx') : $item->textcx}}</textarea>
                            </div>

                            <!-- textarea -->
                            <div class="form-group">
                                <label>Texto Farroupilha</label>
                                <textarea class="form-control summernote" rows="3" placeholder="Texto corpo" name="textfr">{{ empty($item->textfr) ? old('textfr') : $item->textfr}}</textarea>
                            </div>

                            <div class="form-group">
                                <input type="submit" value="Salvar" class="btn btn-success btn-send pull-right" />
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
<!-- include summernote css/js -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
@stop
@section('js')
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script>
   $('.summernote').summernote({
        placeholder: 'Hello stand alone ui',
        tabsize: 2,
        height: 150
      });
</script>
@yield('js')
@stop