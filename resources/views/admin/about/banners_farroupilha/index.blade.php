@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
<h1>Banners</h1>
@stop


@section('content')
<div class="row">
    <div class="col-md-8 ">
        <div class="box ">
            <div class="box-header with-border">
                @if (!empty($items))
                <h3 class="box-title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Lista de bannersfr</font></font></h3>
                @else
                <h3 class="box-title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nenhum um item cadastrado</font></font></h3>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
                @endif
                <table id="tabela" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Imagem</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($items as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td><img width="80" src="{{asset('_files/bannersfr/'.$item->image)}}"></td>
                            <td>
                                <div class="btn-group">
                                    <a type="button" class="btn btn-success" href="{{route('admin.bannersfr.editar.{id}', array($item->id))}}" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-pencil"></i></a>
                                    <a type="link"  class="btn btn-danger" href="{{route('admin.bannersfr.delete.{id}', array($item->id))}}" data-toggle="tooltip" data-original-title="Deletar"><i class="fa fa-trash-o"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Imagem</th>
                            <th>Ações</th>
                        </tr>
                    </tfoot>
                </table>
                <!-- /.row -->
            </div>
            <!-- ./box-body -->
        </div>
        <!-- /.box -->
    </div>
    <div class="col-xs-12 col-md-4">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Adicionar Banner</h3>
            </div>
            <form action="{{ route('admin.bannersfr.store') }}" class="form-table-list" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="image">Imagem:</label>
                                <div class="upload">
                                    <div class="btn btn-primary">Adicionar Imagem</div>
                                    <input type="file" class="input-file" id="image" name="file_image" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-success btn-send pull-right">Salvar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>    
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('vendor/adminlte/css/main.css')}}">
@stop

@section('js')
<script>
    $('#tabela').DataTable({
        language: {
            emptyTable: "Nenhum registro encontrado",
            info: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            infoEmpty: "Mostrando 0 até 0 de 0 registros",
            infoFiltered: "(Filtrados de _MAX_ registros)",
            infoPostFix: "",
            infoThousands: ".",
            lengthMenu: "_MENU_ resultados por página",
            loadingRecords: "Carregando...",
            processing: "Processando...",
            zeroRecords: "Nenhum registro encontrado",
            search: "Pesquisar",
            paginate: {
                next: "Próximo",
                previous: "Anterior",
                first: "Primeiro",
                last: "Último"
            },
            aria: {
                sortAscending: ": Ordenar colunas de forma ascendente",
                sortDescending: ": Ordenar colunas de forma descendente"
            }
        }
    });
</script>
@stop