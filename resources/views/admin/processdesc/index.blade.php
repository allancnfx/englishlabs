@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
<h1>Processos</h1>
@stop


@section('content')
<div class="row">
    <div class="col-md-8">
        <div class="box">
            <div class="box-header with-border">
                @if (!empty($items))
                <h3 class="box-title"></h3>
                @else
                <h3 class="box-title">Nenhum um item cadastrado</h3>
                @endif
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
                @endif
                <table id="tabela" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Imagem</th>
                            <th>Descrição</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($items as $item)
                        <tr>
                            <td>{{$item->id}}</td>
                            <td><img width="80" src="{{asset('_files/processos/'.$item->image)}}"></td>
                            <td>{!!$item->desc!!}</td>
                            <td>
                                <div class="btn-group">
                                    <a type="button" class="btn btn-success" href="{{route('admin.processdesc.editar.{id}', array($item->id))}}" data-toggle="tooltip" data-original-title="Editar"><i class="fa fa-pencil"></i></a>
                                    <a type="link"  class="btn btn-danger" href="{{route('admin.processdesc.delete.{id}', array($item->id))}}" data-toggle="tooltip" data-original-title="Deletar"><i class="fa fa-trash-o"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Imagem</th>
                            <th>Descrição</th>
                            <th>Ações</th>
                        </tr>
                    </tfoot>
                </table>
                <!-- /.row -->
            </div>
            <!-- ./box-body -->
        </div>
        <!-- /.box -->
    </div>
    <div class="col-xs-12 col-md-4">
        <div class="box box-info">
            <div class="box-header">
                <h3 class="box-title">Adicionar Banner</h3>
            </div>
            <form action="{{ route('admin.processdesc.store') }}" class="form-table-list" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label for="image">Imagem:</label>
                                <div class="upload">
                                    <div class="btn btn-primary">Adicionar Imagem</div>
                                    <input type="file" class="input-file" id="image" name="file_image" />
                                </div>
                            </div>
                            <!-- textarea -->
                            <div class="form-group">
                                <label>Descrição</label>
                                <textarea class="form-control summernote" rows="3" placeholder="Texto corpo" name="desc"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button class="btn btn-success btn-send pull-right">Salvar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>    
@endsection

@section('css')
<link rel="stylesheet" href="{{asset('vendor/adminlte/css/main.css')}}">
<!-- include summernote css/js -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
<!-- include summernote css/js -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
@stop
@section('js')
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script>
   $('.summernote').summernote({
        placeholder: 'Hello stand alone ui',
        tabsize: 2,
        height: 150
      });
</script>
@yield('js')
<script>
    $('#tabela').DataTable({
        language: {
            emptyTable: "Nenhum registro encontrado",
            info: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            infoEmpty: "Mostrando 0 até 0 de 0 registros",
            infoFiltered: "(Filtrados de _MAX_ registros)",
            infoPostFix: "",
            infoThousands: ".",
            lengthMenu: "_MENU_ resultados por página",
            loadingRecords: "Carregando...",
            processing: "Processando...",
            zeroRecords: "Nenhum registro encontrado",
            search: "Pesquisar",
            paginate: {
                next: "Próximo",
                previous: "Anterior",
                first: "Primeiro",
                last: "Último"
            },
            aria: {
                sortAscending: ": Ordenar colunas de forma ascendente",
                sortDescending: ": Ordenar colunas de forma descendente"
            }
        }
    });
</script>
@stop