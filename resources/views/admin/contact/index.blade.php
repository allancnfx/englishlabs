@extends('adminlte::page')

@section('title', 'AdminLTE')


@section('content_header')
<h1>Contato</h1>
@stop


@section('content')
<div class="row">
    <div class="col-md-8 ">
        <div class="box ">
            <div class="box-header with-border">

                <div class="box box-warning">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="{{ empty($item->id) ? route('admin.contact.store') : route('admin.contact.store.{id}', array($item->id)) }}" class="form-table-list" method="POST" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <!-- text input -->
                            <div class="form-group">
                                <label>Título contato</label>
                                <input type="text" class="form-control" placeholder="Título" name="title" value="{{ empty($item->title) ? old('title') : $item->title}}">
                            </div>
                            <!-- textarea -->
                            <div class="form-group">
                                <label>Descrição contato</label>
                                <textarea class="form-control summernote" rows="3" placeholder="Texto corpo" name="contact_desc">{{ empty($item->contact_desc) ? old('contact_desc') : $item->contact_desc}}</textarea>
                            </div>
                            <div class="form-group" >
                                <label>Título Descrição</label>
                                <textarea class="form-control summernote" rows="3" placeholder="Texto corpo" name="titledesc">{{ empty($item->titledesc) ? old('titledesc') : $item->titledesc}}</textarea>
                            </div>
                              <div class="form-group" >
                                <label>Título Formulario</label>
                                <textarea class="form-control summernote" rows="3" placeholder="Texto corpo" name="titleform">{{ empty($item->titleform) ? old('titleform') : $item->titleform}}</textarea>
                            </div>

                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" class="form-control" placeholder="Título" name="email" value="{{ empty($item->email) ? old('email') : $item->email}}">
                            </div>

                             <div class="form-group">
                                <label>whatss</label>
                                <input type="text" class="form-control" placeholder="WhatsApp" name="whatss" value="{{ empty($item->whatss) ? old('whatss') : $item->whatss}}">
                            </div>
                            <div class="form-group">
                                <label>Endereço Caxias</label>
                                 <textarea class="form-control summernote" rows="3" placeholder="Endereço Caxias" name="adress_cx">{{ empty($item->adress_cx) ? old('adress_cx') : $item->adress_cx}}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Telefone Caxias</label>
                                <input type="text" class="form-control" placeholder="Título" name="tel_cx" value="{{ empty($item->tel_cx) ? old('tel_cx') : $item->tel_cx}}">
                            </div>
                            <div class="form-group">
                                <label>Endereço Farroupilha</label>
                                <input type="text" class="form-control" placeholder="Endereço Farroupilha" name="adress_fr" value="{{ empty($item->adress_fr) ? old('adress_fr') : $item->adress_fr}}">
                            </div>
                            <div class="form-group">
                                <label>Telefone Farroupilha</label>
                                <input type="text" class="form-control" placeholder="Título" name="tel_fr" value="{{ empty($item->tel_fr) ? old('tel_fr') : $item->tel_fr}}">
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Salvar" class="btn btn-success btn-send pull-right" />
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
<!-- include summernote css/js -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
@stop

@section('js')
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script>
   $('.summernote').summernote({
        placeholder: 'Hello stand alone ui',
        tabsize: 2,
        height: 150
      });
</script>
@yield('js')
@stop