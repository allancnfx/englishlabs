@extends('adminlte::page')

@section('title', 'AdminLTE')

@section('content_header')
<h1>Nossos Processos</h1>
@stop


@section('content')
<div class="row">
    <div class="col-md-8 ">
        <div class="box ">
            <div class="box-header with-border">

                <div class="box box-warning">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form action="{{ empty($item->id) ? route('admin.process.store') : route('admin.process.store.{id}', array($item->id)) }}" class="form-table-list" method="POST" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <!-- text input -->
                            <div class="form-group">
                                <label>Título</label>
                                <input type="text" class="form-control" placeholder="Título" name="title" value="{{ empty($item->title) ? old('title') : $item->title}}">
                            </div>
                            <!-- textarea -->
                            <div class="form-group">
                                <label>Descrição</label>
                                <textarea class="form-control summernote" rows="3" placeholder="Texto corpo" name="desc">{{ empty($item->desc) ? old('desc') : $item->desc}}</textarea>
                            </div>
                            
                            <div class="form-group">
                                <input type="submit" value="Salvar" class="btn btn-success btn-send pull-right" />
                            </div>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('css')
<!-- include summernote css/js -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
@stop
@section('js')
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>
<script>
   $('.summernote').summernote({
        placeholder: 'Hello stand alone ui',
        tabsize: 2,
        height: 150
      });
</script>
@yield('js')
@stop