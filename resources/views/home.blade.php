@extends('layouts.app')
 
@section('content')
     <section class="welcome-area wow fadeInUp" id="home">
        <!-- Main Content -->
            <!-- Scroll Down Line -->
            <a href="#sobre" class="scroll-down scroll-about">
                <div class="container">
                  <div class="chevron"></div>
                  <div class="chevron"></div>
                  <div class="chevron"></div>
                </div>
            </a>
            <!-- Hover List -->
            <div class="hover-list py-12 px-6">
                <!-- Hover List MEDIA -->
                <div class="hover-list-media swiper" data-effect="fade">
                    @foreach($data['banners'] as $banner)
                        @if(!empty($banner))
                            <!-- Media Item -->
                            <div class="swiper-slide">
                                <div class="slide-media" data-poster="{{asset('_files/banners/'.$banner->image)}}"></div>
                            </div>
                            <!-- /Media Item -->
                        @endif
                    @endforeach
                </div>
                <!-- /Hover List MEDIA -->
                <!-- Hover List TITLES -->
                <div class="container text-light text-center px-0 ">
                    <div class="hover-list-links hover-list-grid row no-gutters">
                        <!-- Title Item -->
                        <div class="item col-lg-6">
                            <a href="#sobre-caxias">
                                <h1>Unidade</h1>
                                <h5 class="item-title">Caxias do Sul</h5>
                            </a>
                        </div>
                        <!-- /Title Item -->
                        <!-- Title Item -->
                        <div class="item col-lg-6">
                            <a href="#sobre-farroupilha">
                                <h1>Unidade</h1>
                                <h5 class="item-title">Farroupilha</h5>
                            </a>
                        </div>
                        <!-- /Title Item -->
                    </div>
                </div>
                <!-- /Hover List TITLES -->
            </div>
            <!-- /Hover List -->
     </section>

    <section class="section-full parallax-window" id="sobre" data-z-index="2" data-parallax="scroll" data-image-src="{{asset('front/images/bg-quem-somos.png') }}" data-natural-width="1200" data-natural-height="800">
        <div class="container">
            <div class="row justify-content-end">
                <div class="col-md-4 col-xs-12 text">
                    <h1 class="title-white">QUEM SOMOS</h1>
                    @if(!empty($data['about']))
                        <div class="heading-white">
                            <p>
                                {!! $data['about']->text_about !!}
                            </p>
                        </div>
                    @endif
                </div>
                <div class="col-sm-1 justify-content-end"></div>
            </div>
        </div>
    </section>
    <!--Documentation area start-->
    <section class="nossos-processos wow" id="nossos-processos">
       <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="heading heading-purple">
                        @if(!empty($data['processtext']->title))
                            <h1>{{ $data['processtext']->title }}</h1>
                        @endif
                        <div class="space-10"></div>
                        @if(!empty($data['processtext']->desc))
                            <p class="white">{!! $data['processtext']->desc !!}</p>
                        @endif
                    </div>
                    <div class="space-60"></div>
                </div>
            </div>
            <div class="row justify-content-center">
                @if(!empty($data['processitems']))
                    @foreach($data['processitems'] as $processitem)
                       <div class="col-6 col-md-6 col-lg-2">
                          <div class="single-document">
                            <div class="document-flag text-center">
                                <img class="img-desc justify-content-center" src="{{asset('_files/processos/'.$processitem->image)}}" alt="">
                            </div>
                          </div>
                          @if(!empty($processitem->desc))
                              <div class="descricao">
                                <p>{!! $processitem->desc !!}</p>
                              </div>
                          @endif
                       </div>
                    @endforeach
                @endif
            </div>
            <div class="row justify-content-center">
                @if(!empty($data['processgrafic']))
                    @foreach($data['processgrafic'] as $processgrafic)
                       <div class="col-12">
                            <img class="item-grafic justify-content-center" src="{{asset('_files/processos/'.$processgrafic->image)}}" alt="">
                       </div>
                    @endforeach
                @endif
            </div>
       </div>
    </section>

    <section class="proficiencia wow" id="proficiencia">
       <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="heading-white">
                        @if(!empty($data['proficiencytext']->title))
                            <h1 class="title-white">{{ $data['proficiencytext']->title }}</h1>
                        @endif
                        <div class="space-10"></div>
                        @if(!empty($data['proficiencytext']->desc))
                            <p class="white">{!! $data['proficiencytext']->desc !!}</p>
                        @endif
                    </div>
                    <div class="space-60"></div>
                </div>
            </div>

            <div class="row justify-content-center">
                @if(!empty($data['proficiencyitems']))
                    @foreach($data['proficiencyitems'] as $proficiencyitem)
                       <div class="col-6 col-md-6 col-lg-2">
                          <div class="single-document">
                            @if(!empty($proficiencyitem->popup))
                                <div class="pop-modal animated fadeInUp">
                                    <div class="overlay"></div>
                                    <div id="pop-text" class="pop-text animated fadeInDown">
                                        <div class="closed">X</div>
                                        <div class="text-popup heading-purple">
                                            <h3>{!! $proficiencyitem->desc !!}</h3>
                                            {!! $proficiencyitem->popup !!}
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <a href="" class="btn-open-modal">
                                <div class="document-flag text-center">
                                    <img class="img-desc justify-content-center" src="{{asset('_files/proficiencia/'.$proficiencyitem->image)}}" alt="">
                                </div>
                            </a>
                          </div>
                          @if(!empty($proficiencyitem->desc))
                              <div class="descricao">
                                <p>{!! $proficiencyitem->desc !!}</p>
                              </div>
                          @endif
                       </div>
                    @endforeach
                @endif
            </div>
        
            <div class="row call">
                <div class="space-10"></div>
                @if(!empty($data['proficiencytext']->call))
                    <p class="white title-white"">{!! $data['proficiencytext']->call !!}</p>
                @endif
            </div>
            <div class="space-60"></div>
        </div>   
    </section>

    <section class="pelo-mundo" id="pelo-mundo">
        <div class="container">
             <div class="row">
                <div class="col-12 text-center">
                    <div class="heading heading-purple">
                        @if(!empty($data['pelomundotext']->title))
                            <h1>{{ $data['pelomundotext']->title }}</h1>
                        @endif
                        <div class="space-10"></div>
                        @if(!empty($data['pelomundotext']->desc))
                            <p class="purple">{!! $data['pelomundotext']->desc !!}</p>
                        @endif
                    </div>
                    <div class="space-60"></div>
                </div>
             </div>
        </div>
         <div class="map-container">
            <div id="mapdiv" class="mapdiv"></div>
         </div>
    </section>

    <section class="section-staff" id="staff">
        <div class="team-area wow fadeInUp section-padding" id="team">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <div class="heading heading-white">
                            @if(!empty($data['stafftext']->title))
                                <h1 class="title-white">{{ $data['stafftext']->title }}</h1>
                            @endif
                            <div class="space-10"></div>
                            @if(!empty($data['stafftext']->desc))
                                <p class="white">{!! $data['stafftext']->desc !!}</p>
                            @endif
                        </div>
                        <div class="space-60"></div>
                    </div>
                </div>
                <div class="row text-center">
                    @if(!empty($data['staffs']))
                        @foreach($data['staffs'] as $staff)
                            <div class="col-6 col-md-4 col-lg-3 item-staff">
                                <div class="single-team">
                                    <div class="single-team-img">
                                        <img src="{{asset('_files/staff/'.$staff->image)}}" alt="">
                                    </div>
                                    <div class="space-30"></div>
                                    <div class="single-team-content">
                                        @if(!empty($staff->title))
                                            <h3 class="title-white">{{ $staff->title  }}</h3>
                                        @endif
                                        <div class="space-10"></div>
                                        @if(!empty($staff->desc))
                                            <h6 class="title-white">{{ $staff->desc }}</h6>
                                        @endif
                                    </div>
                                    <div class="space-10"></div>
                                    <div class="single-team-social">
                                        <ul>
                                            @if(!empty($staff->link_face))
                                                <li><a href="{{$staff->link_face}}" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                            @endif
                                            @if(!empty($staff->link_insta))
                                                <li><a href="{{$staff->link_insta}}" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>

    <section class="" id="sobre-caxias">
            <div class="scroll-down scroll-right">
              <div class="chevron"></div>
              <div class="chevron"></div>
              <div class="chevron"></div>
            </div>

            <div class="swiper hero-fullscreen lightbox" data-slides-per-view="auto" data-mousewheel="false" data-keyboard="true">
                <figure class="swiper-slide bg-light vw-50 d-flex align-items-center">
                    <figcaption class="p-7 heading-white">
                        <h1 class="title-white">UNIDADE</h1>
                        <h3 class="bold uppercase align-items-center title-white">CAXIAS DO SUL</h3>
                         @if(!empty($data['about']))
                            <p>
                                {!! $data['about']->textcx !!}
                            </p>
                        @endif
                    </figcaption>
                </figure>
                @if(!empty($data['bannerscx']))
                    @foreach($data['bannerscx'] as $bannercx)
                        <figure class="swiper-slide portfolio-item vw-50" data-background="{{asset('_files/bannerscx/'.$bannercx->image)}}">
                            <a href="{{asset('_files/bannerscx/'.$bannercx->image)}}" class="lightbox-link" title=""></a>
                        </figure>
                    @endforeach
                @endif
            </div>
    </section>

    <section class="" id="sobre-farroupilha">
            <div class="scroll-down scroll-left">
              <div class="chevron"></div>
              <div class="chevron"></div>
              <div class="chevron"></div>
            </div>

            <div class="swiper hero-fullscreen lightbox" data-slides-per-view="auto" data-initial-slide="9" data-mousewheel="false" data-keyboard="true">
                @if(!empty($data['bannersfr']))
                    @foreach($data['bannersfr'] as $bannerfr)

                        <figure class="swiper-slide portfolio-item vw-50" data-background="{{asset('_files/bannersfr/'.$bannerfr->image)}}">
                            <a href="{{asset('_files/bannersfr/'.$bannerfr->image)}}" class="lightbox-link" title=""></a>
                        </figure>
                    @endforeach
                @endif
                 <figure class="swiper-slide bg-gallery-2 vw-50 d-flex align-items-center">
                    <figcaption class="p-7 heading-white">
                        <h1 class="bold uppercase align-items-center title-white">UNIDADE</h1>
                        <h3 class="bold uppercase title-white">FARROUPILHA</h3>
                        @if(!empty($data['about']))
                            <div class="white">
                                <p>{!! $data['about']->textfr !!}</p>
                            </div>
                        @endif
                    </figcaption>
                </figure>
            </div>
    </section>

    <section class="bg-degrade">
        <section id="depoimentos" class="depoimentos">
             <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <div class="heading heading-white">
                            @if(!empty($data['depositionstext']->title))
                                <h1 class="title-white">{{ $data['depositionstext']->title }}</h1>
                            @endif
                            <div class="space-10"></div>
                            @if(!empty($data['depositionstext']->desc))
                            <div class="heading-white">
                                    <p class="white">{!! $data['depositionstext']->desc !!}</p>
                            </div>
                            @endif
                        </div>
                        <div class="space-60"></div>
                    </div>
                </div>
                <div class="main">
                    <div class="hero split-slider">
                        <div class="row no-gutters">
                            <div class="col-lg-6">
                                <div class="swiper bg-lines" data-swiper-name="captions" data-sync="images" data-mousewheel="false" data-keyboard="true" data-autoplay="4000" data-pagination="true" data-loop="true">
                                    @if(!empty($data['depositionsdesc']))
                                        @foreach($data['depositionsdesc'] as $deposition)
                                            <div class="swiper-slide d-flex align-items-center justify-content-center text-center h-100 px-8">
                                                <div class="inner">
                                                    @if(!empty($deposition->title))
                                                        <h1 class="bold uppercase">{{ $deposition->title }}</h1>
                                                    @endif
                                                    @if(!empty($deposition->desc))
                                                        <p>{!! $deposition->desc !!}</p>
                                                    @endif
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="swiper" data-sync="captions" data-swiper-name="images" data-mousewheel="false" data-effect="fade" data-loop="true" >
                                    @if(!empty($data['depositionsdesc']))
                                        @foreach($data['depositionsdesc'] as $deposition)
                                        <div class="swiper-slide" data-background="{{asset('_files/depoimentos/'.$deposition->image)}}"></div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="eventos" class="eventos">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center">
                        <div class="heading heading-white">
                            @if(!empty($data['eventtext']->title))
                                <h1>{{ $data['eventtext']->title }}</h1>
                            @endif
                            <div class="space-10"></div>
                            @if(!empty($data['eventtext']->desc))
                                <p class="white">{!! $data['eventtext']->desc !!}</p>
                            @endif
                        </div>
                        <div class="space-60"></div>
                    </div>
                </div>
                <div class="setas">
                    <div class="container">
                        <div class="row">
                            <div class="col-6 col-md-6">
                                <div class="scroll-down scroll-left">
                                    <div class="chevron"></div>
                                    <div class="chevron"></div>
                                    <div class="chevron"></div>
                                </div>
                            </div>
                            <div class="col-6 col-md-6">
                                <div class="scroll-down scroll-right" >
                                    <div class="chevron"></div>
                                    <div class="chevron"></div>
                                    <div class="chevron"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="swiper-container" data-mousewheel="false" data-keyboard="true" data-autoplay="4000" data-pagination="true" data-loop="true">
                    <div class="swiper-wrapper">
                        @if(!empty($data['events']))
                            @foreach($data['events'] as $event)
                                <div class="swiper-slide">
                                    <div class="box-event" style="background-image:url({{asset('_files/eventos/'.$event->image)}})">
                                        <figure>
                                            <figcaption>
                                                <div class="item-caption-inner">
                                                    <p class="text-links">
                                                        <a href="#">{{ $event->data }}</a>
                                                    </p>
                                                    <h3 class="bold uppercase">{{ $event->desc }}</h3>
                                                    <p>
                                                        {!! $event->text !!}
                                                    </p>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </section>

    <section id="contato" class="contato">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <div class="heading-purple">
                        @if(!empty($data['contatotext']->title))
                            <h1>{{ $data['contatotext']->title }}</h1>
                        @endif
                        <div class="space-10"></div>
                        @if(!empty($data['contatotext']->contact_desc))
                            <p>{!! $data['contatotext']->contact_desc !!}</p>
                        @endif
                    </div>
                    <div class="space-60"></div>
                </div>
            </div>
            <div class="row p-contato">
                <div class="col-md-5">
                    @if(!empty($data['contatotext']->titledesc))
                        <h5 class="my-5">{!! $data['contatotext']->titledesc !!}</h5>
                    @endif
                     <p class="text-contat"><b>CAXIAS DO SUL
                                @if(!empty($data['contatotext']->tel_cx))
                                 -  {{$data['contatotext']->tel_cx}}</b></p>
                                @endif
                            <div class="adress_cx">
                                <p>
                                    @if(!empty($data['contatotext']->adress_cx))
                                         {!!$data['contatotext']->adress_cx!!}
                                    @endif
                                </p>
                            </div>
                            <p class="text-contat"><b>FARROUPILHA
                                @if(!empty($data['contatotext']->tel_fr))
                                - {{$data['contatotext']->tel_fr}}</b></p>                          
                                @endif
                            <p> 
                                @if(!empty($data['contatotext']->adress_fr))
                                   {{$data['contatotext']->adress_fr}}
                                @endif
                            </p>
                    

                    @if(!empty($data['contatotext']->whatss))
                        <p><b>WhatsApp</b> {{$data['contatotext']->whatss}}</p>
                    @endif
                    
                    @if(!empty($data['contatotext']->email))
                        <p><b>EMAIL:</b> {{$data['contatotext']->email}}</p>
                    @endif
                    
                </div>
                <div class="col-md-7">
                    <div class="mail-form-message d-none mb-5"></div>
                        <div class="box-header with-border">
                          @if(Session::has('success'))
                                    <div class="send-success" role="alert">{!! Session::get('success') !!}</div>
                                @endif
                                @if(Session::has('error'))
                                    <div class="send-error" role="alert">{!! Session::get('error') !!}</div>
                                @endif
                                @if (count($errors) > 0)
                                    <div class="send-error" role="alert">Falha no Envio! Verifique os campos obrigatórios e tente novamente.</div>
                                @endif
                        </div>
                    <form class="mail-form" method="POST" action="{{route('send')}}">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-md-12 ">
                                  @if(!empty($data['contatotext']->titleform))
                                <h3 class="uppercase center-block">{!! $data['contatotext']->titleform !!}</h3>
                                @endif
                            </div>

                            <div class="col-md-6">
                                <input name="name" type="text" placeholder="Nome:" required>
                            </div>
                            <div class="col-md-6">
                                <input name="phone" type="text" placeholder="Telefone:" required>
                            </div>
                            <div class="col-md-12">
                                <input name="email" type="email" placeholder="Email:" required>
                                <textarea name="text" placeholder="Mensagem:" required rows="4"></textarea>
                                <button type="submit" class="button style-5 uppercase button-contato">Enviar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="googlemaps">
            <div class="col-md-12 mp-maps">
                <div class="row">
                    <div class="col-md-6 text-center">
                        <p>FAÇA UM TOUR 360º PELA NOSSA CASINHA EM CAXIAS DO SUL</p>
                    </div>
                    <div class="col-md-6">
                        <p></p>
                    </div>
                    <div class="col-md-6 pad-maps">
                         <iframe src="https://www.google.com/maps/embed?pb=!4v1537293161997!6m8!1m7!1sCAoSLEFGMVFpcE10NHhfOTc1WG9fRmN2Q3hkZTBqcm5ISmtMdnJIUE5xM1FoeDNq!2m2!1d-29.170614566356!2d-51.18602981209!3f248.1120708525629!4f1.2449362208804047!5f0.7820865974627469" height="450" frameborder="0" style="border:0;width:100%;" allowfullscreen></iframe>
                    </div>
                    <div class="col-md-6 pad-maps">
                        <div id="map" style="height: 450px;"></div>
                    </div>
                    <script src="http://maps.google.com/maps/api/js?key=AIzaSyCtwWwyEUBEqnW0dXhl3tGlqOje5YwCtHA&sensor=false" type="text/javascript"></script>
                    <script type="text/javascript">
                        var locations = [
                          ['English Caxias do Sul', -29.170626, -51.186049, 4],
                          ['English Farroupilha', -29.2228025, -51.3445251, 4]
                        ];
                        var map = new google.maps.Map(document.getElementById('map'), {
                          zoom: 11,
                          center: new google.maps.LatLng(-29.201666, -51.273508),
                          mapTypeId: google.maps.MapTypeId.ROADMAP
                        });

                        var infowindow = new google.maps.InfoWindow();

                        var marker, i;

                        for (i = 0; i < locations.length; i++) {
                          marker = new google.maps.Marker({
                            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                            icon: 'http://www.englishlabs.com.br/front/images/favicon-map.png',
                            map: map
                          });

                          google.maps.event.addListener(marker, 'click', (function(marker, i) {
                            return function() {
                              infowindow.setContent(locations[i][0]);
                              infowindow.open(map, marker);
                            }
                          })(marker, i));
                        }
                    </script>                        
                </div>
            </div>
        </div>
    </section>
   

     <div id="bio_ep" class="bio_ep">
        <div class="popup-form">
       <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="mail-form-message d-none mb-5"></div>
                        <div class="box-header with-border">
                          @if(Session::has('success'))
                                    <div class="send-success" role="alert">{!! Session::get('success') !!}</div>
                                @endif
                                @if(Session::has('error'))
                                    <div class="send-error" role="alert">{!! Session::get('error') !!}</div>
                                @endif
                                @if (count($errors) > 0)
                                    <div class="send-error" role="alert">Falha no Envio! Verifique os campos obrigatórios e tente novamente.</div>
                                @endif
                        </div>
                        <form class="mail-form" method="POST" action="{{route('send')}}">
                            {!! csrf_field() !!}
                            <div class="row">
                                <div class="col-md-6">
                                    <input name="name" type="text" placeholder="Nome:" required>
                                </div>
                                <div class="col-md-6">
                                    <input name="phone" type="text" placeholder="Telefone:" required>
                                </div>
                                <div class="col-md-12">
                                    <input name="email" type="email" placeholder="Email:" required>
                                    <textarea name="text" placeholder="Mensagem:" required rows="4"></textarea>
                                    <button type="submit" class="button style-5 uppercase button-contato">Enviar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                </div>
        </div>
    </div>

    <div class="pop-modal-mobile">
        <div class="overlay-mobile"></div>
        <div class="pop-text-mobile">
            <a class="closed"><div class="closed-mobile">X</div><a>
            <div class="text-popup-mobile heading-purple">
            @if(!empty($data['popup']->desc_mobile))
                <h3>{!! $data['popup']->desc_mobile !!}<h3>
            @endif
            </div>
        </div>
    </div>
    
   
    
@stop

