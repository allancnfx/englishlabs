<table width="600" cellpadding="5" cellspacing="0">
    <tbody>
        <tr>
            <td>
                Solicitação de contato.
            </td>
        </tr>
        <tr>
            <td width="100" align="right">Nome:</td>
            <td>{{ $requestName }}</td>
        </tr>
        <tr>
            <td width="100" align="right">Telefone:</td>
            <td>{{ $requestPhone }}</td>
        </tr>
        <tr>
            <td width="100" align="right">Email:</td>
            <td>{{ $requestEmail }}</td>
        </tr>
        <tr>
            <td width="100" align="right">Mensagem:</td>
            <td>{{ $requestText }}</td>
        </tr>
    </tbody>
</table>